package com.example.project0405level1

fun main() {
    val list: MutableList<String>

    print("Введите количество номеров которые вы хотите ввести: ")
    val numberCount = readLine()?.toIntOrNull() ?: return

    list = arrayListCreating(numberCount)
    val filteredList = list.filter {it.startsWith("+7")}.toMutableList()
    println("Ваш список: $filteredList")

    println("Exclusive numbers: ${foundOutExclusiveNumbers(filteredList)}")

    println("Sum length of all numbers: ${sunBy(filteredList)}")

    for (element in createMutableMap(filteredList)) {
        println("Человек: ${element.key}. Номер телефона: ${element.value}.")
    }
}

fun arrayListCreating(n: Int): MutableList<String> {
    val list: MutableList<String> = mutableListOf()
    var count: Int = 0
    while(count < n) {
        print("Введите номер начиная с +7: ")
        val phoneNumber = readLine()?.toString() ?: continue
        list.add(phoneNumber)
        count++
    }
    return list
}

fun foundOutExclusiveNumbers(list: MutableList<String>): Int {
    val set: MutableSet<String> = mutableSetOf()
    for (string in list) {
        set.add(string)
    }
    return set.size
}

fun sunBy(list: MutableList<String>): Int {
    var finalSum = 0
    for(size in list) {
        finalSum += size.length
    }
    return finalSum
}

fun createMutableMap(list: MutableList<String>): MutableMap<String, String> {
    val mutableMap: MutableMap<String, String> = mutableMapOf()
    for(element in list) {
        println("Введите имя человека с номером телефона $element :")
        val nameOfOwner = readLine()?.toString() ?: continue
        mutableMap[nameOfOwner] = element

    }
    return mutableMap
}

